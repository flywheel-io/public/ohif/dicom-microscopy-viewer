import Draw, { DrawEvent } from 'ol/interaction/Draw.js';
import GeometryType from 'ol/geom/GeometryType.js';
import MultiLineString from 'ol/geom/MultiLineString.js';
import MultiPoint from 'ol/geom/MultiPoint.js';
import MultiPolygon from 'ol/geom/MultiPolygon.js';

const Mode = {
    POINT: 'Point',
    LINE_STRING: 'LineString',
    POLYGON: 'Polygon',
    CIRCLE: 'Circle',
};

/**
 * @enum {string}
 */
const DrawEventType = {
    /**
     * Triggered upon feature draw start
     * @event DrawEvent#drawstart
     * @api
     */
    DRAWSTART: 'drawstart',
    /**
     * Triggered upon feature draw end
     * @event DrawEvent#drawend
     * @api
     */
    DRAWEND: 'drawend',
    /**
     * Triggered upon feature draw abortion
     * @event DrawEvent#drawabort
     * @api
     */
    DRAWABORT: 'drawabort',
};

export default class DrawInteraction extends Draw {  
    /**
     * Stop drawing and add the sketch feature to the target layer.
     * The {@link module:ol/interaction/Draw~DrawEventType.DRAWEND} event is
     * dispatched before inserting the feature.
     * @api
     */
    finishDrawing() {
        const sketchFeature = this.abortDrawing_();
        if (!sketchFeature) {
            return;
        }
        let coordinates = this.sketchCoords_;
        const geometry = sketchFeature.getGeometry();
        const projection = this.getMap().getView().getProjection();
        if (this.mode_ === Mode.LINE_STRING) {
            // remove the redundant last point
            if (coordinates.length > 2) {
                coordinates.pop();
            }
            this.geometryFunction_(coordinates, geometry, projection);
        }
        else if (this.mode_ === Mode.POLYGON) {
            // remove the redundant last point in ring
            /** @type {PolyCoordType} */ (coordinates)[0].pop();
            this.geometryFunction_(coordinates, geometry, projection);
            coordinates = geometry.getCoordinates();
        }
        // cast multi-part geometries
        if (this.type_ === GeometryType.MULTI_POINT) {
            sketchFeature.setGeometry(new MultiPoint([/** @type {PointCoordType} */ (coordinates)]));
        }
        else if (this.type_ === GeometryType.MULTI_LINE_STRING) {
            sketchFeature.setGeometry(new MultiLineString([/** @type {LineCoordType} */ (coordinates)]));
        }
        else if (this.type_ === GeometryType.MULTI_POLYGON) {
            sketchFeature.setGeometry(new MultiPolygon([/** @type {PolyCoordType} */ (coordinates)]));
        }
        // First dispatch event to allow full set up of feature
        this.dispatchEvent(new DrawEvent(DrawEventType.DRAWEND, sketchFeature));
        // Then insert feature
        if (this.features_) {
            this.features_.push(sketchFeature);
        }
        if (this.source_) {
            this.source_.addFeature(sketchFeature);
        }
    };
}